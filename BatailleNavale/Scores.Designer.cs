﻿namespace BatailleNavale
{
    partial class Scores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.retour = new System.Windows.Forms.Label();
            this.best = new System.Windows.Forms.Label();
            this.best2 = new System.Windows.Forms.Label();
            this.best3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 49.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 690);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(233, 76);
            this.label1.TabIndex = 0;
            this.label1.Text = "Retour";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(277, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(522, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "Vous pouvez consulter ici vos meilleurs scores !";
            // 
            // retour
            // 
            this.retour.AutoSize = true;
            this.retour.BackColor = System.Drawing.Color.Transparent;
            this.retour.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.retour.Location = new System.Drawing.Point(428, 569);
            this.retour.Name = "retour";
            this.retour.Size = new System.Drawing.Size(225, 73);
            this.retour.TabIndex = 2;
            this.retour.Text = "Retour";
            this.retour.Click += new System.EventHandler(this.retour_Click);
            // 
            // best
            // 
            this.best.AutoSize = true;
            this.best.BackColor = System.Drawing.Color.Transparent;
            this.best.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.best.Location = new System.Drawing.Point(275, 193);
            this.best.Name = "best";
            this.best.Size = new System.Drawing.Size(447, 39);
            this.best.TabIndex = 3;
            this.best.Text = "Meilleur score : Pas attribué";
            // 
            // best2
            // 
            this.best2.AutoSize = true;
            this.best2.BackColor = System.Drawing.Color.Transparent;
            this.best2.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.best2.Location = new System.Drawing.Point(275, 301);
            this.best2.Name = "best2";
            this.best2.Size = new System.Drawing.Size(574, 39);
            this.best2.TabIndex = 4;
            this.best2.Text = "Second meilleur score : Pas attribué";
            // 
            // best3
            // 
            this.best3.AutoSize = true;
            this.best3.BackColor = System.Drawing.Color.Transparent;
            this.best3.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.best3.Location = new System.Drawing.Point(275, 405);
            this.best3.Name = "best3";
            this.best3.Size = new System.Drawing.Size(611, 39);
            this.best3.TabIndex = 5;
            this.best3.Text = "Troisième meilleur score : Pas attribué";
            // 
            // Scores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.BackgroundImage = global::BatailleNavale.Properties.Resources.fond;
            this.ClientSize = new System.Drawing.Size(1149, 680);
            this.Controls.Add(this.best3);
            this.Controls.Add(this.best2);
            this.Controls.Add(this.best);
            this.Controls.Add(this.retour);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Scores";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Scores";
            this.Load += new System.EventHandler(this.load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label retour;
        private System.Windows.Forms.Label best;
        private System.Windows.Forms.Label best2;
        private System.Windows.Forms.Label best3;
    }
}