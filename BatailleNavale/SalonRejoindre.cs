﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;

namespace BatailleNavale
{
    public partial class SalonRejoindre : Form
    {
        public SalonRejoindre()
        {
            InitializeComponent();
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.exit);
        }

        private void exit(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            String ip = textBox1.Text;
            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(ip), 7000);
            client.Connect(serverEndPoint);
            Jeu form = new Jeu();
            form.setType("Client");
            form.setClient(client);
            form.Show();
            this.Hide();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            JouerMenu jm = new JouerMenu();
            jm.Show();
            this.Hide();
        }
    }
}
