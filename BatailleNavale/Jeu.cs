﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;
using System.IO;

namespace BatailleNavale
{
    public partial class Jeu : Form
    {
        private Bateau porteAvion = null, croiseur = null, contreTorpilleur = null, torpilleur = null, sousMarin = null;
        private int orientation = 1;
        private String type;
        private TcpClient client;
        private Boolean tour;
        private Boolean stop = false;
        private TcpListener tcpServer;
        private Thread rt;
        private int nombreTour = 0, bateauDetruit = 0, bateauAllieDetruit = 0;

        public Jeu()
        {
            InitializeComponent();
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.exit);

            updateMouse();
        }

        private void exitGame()
        {
            client.Close();
            if (type.Equals("Server"))
                tcpServer.Stop();
            rt.Abort();
        }

        private void exit(object sender, FormClosingEventArgs  e)
        {
            exitGame();
            Application.Exit();
        }

        public void setServer(TcpListener server)
        {
            tcpServer = server;
        }

        public void afficherBateau(Bateau bateau, System.Drawing.Bitmap image)
        {
            Panel bateauPanel = new Panel();
            bateauPanel.Location = new System.Drawing.Point((bateau.getCaseDepartX() - 1) * 54, (bateau.getCaseDepartY() - 1) * 54);
            if (bateau.getOrientation() == 2)
                bateauPanel.Size = new System.Drawing.Size(54, 54 * bateau.getTaille());
            else
                bateauPanel.Size = new System.Drawing.Size(54 * bateau.getTaille(), 54);
            bateauPanel.BackgroundImage = image;
            this.tabPlayer.Controls.Add(bateauPanel);
            bateauPanel.BringToFront();
        }

        private void tabPlayer_Click(object sender, EventArgs e)
        {
            MouseEventArgs eM = (MouseEventArgs)e;
            if (eM.Button == MouseButtons.Right)
            {
                if(orientation == 1)
                    orientation = 2;
                else
                    orientation = 1;
            }
            else if (eM.Button == MouseButtons.Left)
            {
                int X = eM.X;
                int Y = eM.Y;
                int caseX, caseY;
                for (caseX = 0; caseX * 54 < X; caseX++) ;
                for (caseY = 0; caseY * 54 < Y; caseY++) ;
                if (porteAvion == null)
                {
                    if (orientation == 2)
                        caseY -= 2;
                    else if (orientation == 1)
                        caseX -= 2;
                    porteAvion = new Bateau(caseX, caseY, 5, orientation);
                    if (porteAvion.getOrientation() == 2)
                        afficherBateau(porteAvion, Properties.Resources.pa);
                    else if (porteAvion.getOrientation() == 1)
                        afficherBateau(porteAvion, Properties.Resources.pa2);
                }
                else if (croiseur == null)
                {
                    if (orientation == 2)
                        caseY -= 2;
                    else if (orientation == 1)
                        caseX -= 2;
                    croiseur = new Bateau(caseX, caseY, 4, orientation);
                    if (croiseur != null)
                    {
                        if (croiseur.getOrientation() == 2)
                            afficherBateau(croiseur, Properties.Resources.cr);
                        else if (croiseur.getOrientation() == 1)
                            afficherBateau(croiseur, Properties.Resources.cr2);
                    }
                }
                else if (contreTorpilleur == null)
                {
                    if (orientation == 2)
                        caseY -= 1;
                    else if (orientation == 1)
                        caseX -= 1;
                    contreTorpilleur = new Bateau(caseX, caseY, 3, orientation);
                    if (contreTorpilleur != null)
                    {
                        if (contreTorpilleur.getOrientation() == 2)
                            afficherBateau(contreTorpilleur, Properties.Resources.ct);
                        else if (contreTorpilleur.getOrientation() == 1)
                            afficherBateau(contreTorpilleur, Properties.Resources.ct2);
                    }
                }
                else if (torpilleur == null)
                {
                    if (orientation == 2)
                        caseY -= 1;
                    else if (orientation == 1)
                        caseX -= 1;
                    torpilleur = new Bateau(caseX, caseY, 3, orientation);
                    if (torpilleur != null)
                    {
                        if (torpilleur.getOrientation() == 2)
                            afficherBateau(torpilleur, Properties.Resources.to);
                        else if (torpilleur.getOrientation() == 1)
                            afficherBateau(torpilleur, Properties.Resources.to2);
                    }
                }
                else if (sousMarin == null)
                {
                    if (orientation == 2)
                        caseY -= 1;
                    else if (orientation == 1)
                        caseX -= 1;
                    sousMarin = new Bateau(caseX, caseY, 2, orientation);
                    if (sousMarin != null)
                    {
                        if (sousMarin.getOrientation() == 2)
                            afficherBateau(sousMarin, Properties.Resources.sm);
                        else if (sousMarin.getOrientation() == 1)
                            afficherBateau(sousMarin, Properties.Resources.sm2);
                    }
                    NetworkStream clientStream = client.GetStream();
                    BinaryWriter writer = new BinaryWriter(clientStream);
                    writer.Write("ok");
                    Thread okPlacement = new Thread(new ParameterizedThreadStart(coReadBytePlacement));
                    okPlacement.Start(clientStream);
                    okPlacement.IsBackground = true;
                }
            }
            updateMouse();
        }

        private void tabTarget_Click(object sender, EventArgs e)
        {
            if(tour == true)
            {
                MouseEventArgs eM = (MouseEventArgs)e;
                int X = eM.X;
                int Y = eM.Y;
                int caseX, caseY;
                for (caseX = 0; caseX * 54 < X; caseX++) ;
                for (caseY = 0; caseY * 54 < Y; caseY++) ;
                NetworkStream clientStream = client.GetStream();

                BinaryWriter writer = new BinaryWriter(clientStream);
                writer.Write(1);
                writer.Write(caseX);
                writer.Write(caseY);
            }
        }

        public void setType(String type)
        {
            this.type = type;
            this.Text = "Jeu - " + type;
        }

        public void setClient(TcpClient socket)
        {
            this.client = socket;
        }

        private void coReadBytePlacement(object obj)
        {
            this.Invoke((MethodInvoker)delegate()
            {
                instruction.Text = "Votre adversaire fini de placer ses bateaux";
            });
            NetworkStream clientStream = (NetworkStream)obj;
            BinaryReader reader = new BinaryReader(clientStream);
            reader.ReadString();
            this.Invoke((MethodInvoker)delegate()
            {
                if(type.Equals("Server"))
                {
                    tour = true;
                    instruction.Text = "Veuillez cibler une case dans le tableau de droite";
                }
                else
                {
                    tour = false;
                    instruction.Text = "Veuillez patienter pendant que votre adversaire joue";
                }
                rt = new Thread(new ParameterizedThreadStart(reponseThread));
                rt.Start(clientStream);
                rt.IsBackground = true;
            });
        }

        private void updateMouse()
        {
            if (porteAvion == null)
            {
                if (orientation == 2)
                {
                    this.Cursor = new Cursor(Properties.Resources.pa.GetHicon());
                }
                else
                {
                    this.Cursor = new Cursor(Properties.Resources.pa2.GetHicon());
                }
            }
            else if (croiseur == null)
            {
                if (orientation == 2)
                {
                    this.Cursor = new Cursor(Properties.Resources.cr.GetHicon());
                }
                else
                {
                    this.Cursor = new Cursor(Properties.Resources.cr2.GetHicon());
                }
            }
            else if (contreTorpilleur == null)
            {
                if (orientation == 2)
                {
                    this.Cursor = new Cursor(Properties.Resources.ct.GetHicon());
                }
                else
                {
                    this.Cursor = new Cursor(Properties.Resources.ct2.GetHicon());
                }
            }
            else if (torpilleur == null)
            {
                if (orientation == 2)
                {
                    this.Cursor = new Cursor(Properties.Resources.to.GetHicon());
                }
                else
                {
                    this.Cursor = new Cursor(Properties.Resources.to2.GetHicon());
                }
            }
            else if (sousMarin == null)
            {
                if (orientation == 2)
                {
                    this.Cursor = new Cursor(Properties.Resources.sm.GetHicon());
                }
                else
                {
                    this.Cursor = new Cursor(Properties.Resources.sm2.GetHicon());
                }
            }
            else
                this.Cursor = Cursors.Default;
        }

        private void reponseThread(object obj)
        {
            NetworkStream clientStream = (NetworkStream)obj;
            BinaryWriter writer = new BinaryWriter(clientStream);
            BinaryReader reader = new BinaryReader(clientStream);
            while(!stop)
            {
                int recu = reader.ReadInt32();
                if(recu == 1)
                {
                    int x = reader.ReadInt32();
                    int y = reader.ReadInt32();
                    int resultat = 2;
                    if (porteAvion.testTouche(x, y) || croiseur.testTouche(x, y) || contreTorpilleur.testTouche(x, y) || torpilleur.testTouche(x, y) || sousMarin.testTouche(x, y))
                    {
                        resultat = 1;
                        this.Invoke((MethodInvoker)delegate()
                        {
                            bateauAllieDetruit++;
                        });
                    }
                        
                    writer.Write(2);
                    writer.Write(x);
                    writer.Write(y);
                    writer.Write(resultat);
                    this.Invoke((MethodInvoker)delegate()
                    {
                        Panel tirPanel = new Panel();
                        tirPanel.Location = new System.Drawing.Point((x - 1) * 54, (y - 1) * 54);
                        tirPanel.Size = new System.Drawing.Size(54, 54);
                        tirPanel.BackgroundImage = BatailleNavale.Properties.Resources.croix;
                        this.tabPlayer.Controls.Add(tirPanel);
                        tirPanel.BringToFront();
                        tour = true;
                        instruction.Text = "Veuillez cibler une case dans le tableau de droite";
                        if (bateauAllieDetruit == 17)
                        {
                            Resultat resul = new Resultat(false, nombreTour);
                            resul.Show();
                            this.Hide();
                            exitGame();
                        }
                    });
                }
                else if (recu == 2)
                {
                    int x = reader.ReadInt32();
                    int y = reader.ReadInt32();
                    int resultat = reader.ReadInt32();
                    this.Invoke((MethodInvoker)delegate()
                    {
                        nombreTour++;
                        if (resultat == 1)
                            bateauDetruit++;
                        Panel tirPanel = new Panel();
                        tirPanel.Location = new System.Drawing.Point((x - 1) * 54, (y - 1) * 54);
                        tirPanel.Size = new System.Drawing.Size(54, 54);
                        if (resultat == 1)
                            tirPanel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
                        else if (resultat == 2)
                            tirPanel.BackColor = System.Drawing.SystemColors.Highlight;
                        this.tabTarget.Controls.Add(tirPanel);
                        tour = false;
                        instruction.Text = "Veuillez patienter pendant que votre adversaire joue";
                        if (bateauDetruit == 17)
                        {
                            Resultat resul = new Resultat(true, nombreTour);
                            resul.Show();
                            this.Hide();
                            exitGame();
                        }
                    });
                }
                /*else if (recu == 3)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        MessageBox.Show("Votre adversaire a été déconnecté !");
                        Menu menu = new Menu();
                        this.Hide();
                        menu.Show();
                        client.Close();
                        if (type.Equals("Server"))
                            tcpServer.Stop();
                        stop = true;
                        rt.Abort();
                    });
                }*/
            }
        }
    }
}
