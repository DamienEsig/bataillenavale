﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace BatailleNavale
{
    public partial class SalonHeberger : Form
    {
        private TcpListener tcpServer;
        private Thread listenThread;
        private TcpClient client;
        
        public SalonHeberger()
        {
            InitializeComponent();
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.exit);
            tcpServer = new TcpListener(IPAddress.Any, 7000);
            listenThread = new Thread(new ThreadStart(EcouteClient));
            listenThread.Start();
            listenThread.IsBackground = true;
        }

        private void exit(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void EcouteClient()
        {
            tcpServer.Start();
            client = tcpServer.AcceptTcpClient();
            this.Invoke((MethodInvoker)delegate()
            {
                Jeu form = new Jeu();
                form.setType("Server");
                form.setClient(client);
                form.setServer(tcpServer);
                form.Show();
                this.Hide();
            });
        }

        private void label2_Click(object sender, EventArgs e)
        {
            tcpServer.Server.Close();
            listenThread.Abort();
            tcpServer.Stop();
            JouerMenu jm = new JouerMenu();
            jm.Show();
            this.Hide();
        }

        public TcpListener getServer()
        {
            return tcpServer;
        }
    }
}