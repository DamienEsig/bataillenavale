﻿using System;

public class Bateau
{
    private int caseDepartX;

    private int caseDepartY;

    private int orientation;

    private int taille;

    private Boolean[] etat;

	public Bateau(int cdX, int cdY, int tai, int ori)
	{
        orientation = ori;
        caseDepartX = cdX;
        caseDepartY = cdY;
        taille = tai;
        etat = new Boolean[taille];
        for (int i = 0; i < taille; i++)
            etat[i] = false;
	}

    public int getCaseDepartX()
    {
        return caseDepartX;
    }

    public int getCaseDepartY()
    {
        return caseDepartY;
    }

    public int getTaille()
    {
        return taille;
    }

    public Boolean testTouche(int x, int y)
    {
        if (orientation == 1)
        {
            for (int i = caseDepartX; i < caseDepartX + taille; i++)
            {
                if (i == x && caseDepartY == y)
                {
                    return true;
                }
            }  
        }
            
        else if (orientation == 2)
        {
            for (int j = caseDepartY; j < caseDepartY + taille; j++)
            {
                if (caseDepartX == x && j == y)
                {
                    return true;
                }
            }
        }
                    
        return false;
    }

    public int getOrientation()
    {
        return orientation;
    }
}
