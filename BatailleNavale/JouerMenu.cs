﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BatailleNavale
{
    public partial class JouerMenu : Form
    {

        public JouerMenu()
        {
            InitializeComponent();
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.exit);
        }

        private void exit(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
        
        private void labelHeberger_Click(object sender, EventArgs e)
        {
            SalonHeberger sh = new SalonHeberger();
            sh.Show();
            this.Hide();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu();
            menu.Show();
            this.Hide();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            SalonRejoindre sr = new SalonRejoindre();
            sr.Show();
            this.Hide();
        }
    }
}
