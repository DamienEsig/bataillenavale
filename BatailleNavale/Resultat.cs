﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace BatailleNavale
{
    public partial class Resultat : Form
    {
        private MySqlConnection connection;

        public Resultat(Boolean etat, int nombre)
        {
            InitializeComponent();
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.exit);
            if (etat == true)
            {
                this.Text = "Vous avez gagné !";
                this.messageFin.Text = "Vous avez gagné !";
                string connectionString = "SERVER=127.0.0.1; DATABASE=bataillenavale; UID=root; PASSWORD=";
                this.connection = new MySqlConnection(connectionString);
                try
                {
                    // Ouverture de la connexion SQL
                    this.connection.Open();

                    // Création d'une commande SQL en fonction de l'objet connection
                    MySqlCommand cmd = this.connection.CreateCommand();

                    // Requête SQL
                    cmd.CommandText = "INSERT INTO scores (resultat) VALUES (@resultat)";

                    // utilisation de l'objet contact passé en paramètre
                    cmd.Parameters.AddWithValue("@resultat", nombre);

                    // Exécution de la commande SQL
                    cmd.ExecuteNonQuery();

                    // Fermeture de la connexion
                    this.connection.Close();
                }
                catch
                {

                }
            }
            else
            {
                this.Text = "Vous avez perdu !";
                this.messageFin.Text = "Vous avez perdu !";
            }
        }

        private void exit(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }


        private void retour_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu();
            this.Hide();
            menu.Show();
        }
    }
}
