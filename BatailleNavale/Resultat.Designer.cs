﻿namespace BatailleNavale
{
    partial class Resultat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.messageFin = new System.Windows.Forms.Label();
            this.retour = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // messageFin
            // 
            this.messageFin.AutoSize = true;
            this.messageFin.BackColor = System.Drawing.Color.Transparent;
            this.messageFin.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageFin.Location = new System.Drawing.Point(114, 103);
            this.messageFin.Name = "messageFin";
            this.messageFin.Size = new System.Drawing.Size(234, 42);
            this.messageFin.TabIndex = 0;
            this.messageFin.Text = "Vous avez ...";
            // 
            // retour
            // 
            this.retour.AutoSize = true;
            this.retour.BackColor = System.Drawing.Color.Transparent;
            this.retour.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.retour.Location = new System.Drawing.Point(69, 262);
            this.retour.Name = "retour";
            this.retour.Size = new System.Drawing.Size(403, 73);
            this.retour.TabIndex = 1;
            this.retour.Text = "Retour Menu";
            this.retour.Click += new System.EventHandler(this.retour_Click);
            // 
            // Resultat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.BackgroundImage = global::BatailleNavale.Properties.Resources.fond;
            this.ClientSize = new System.Drawing.Size(526, 401);
            this.Controls.Add(this.retour);
            this.Controls.Add(this.messageFin);
            this.Name = "Resultat";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Resultat";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label messageFin;
        private System.Windows.Forms.Label retour;
    }
}