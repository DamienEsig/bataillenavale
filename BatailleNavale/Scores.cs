﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace BatailleNavale
{
    public partial class Scores : Form
    {
        private MySqlConnection connection;

        public Scores()
        {
            InitializeComponent();
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.exit);
        }

        private void exit(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu();
            menu.Show();
            this.Hide();
        }

        private void load(object sender, EventArgs e)
        {
            List<Int32> scores = new List<Int32>();
            string connectionString = "SERVER=127.0.0.1; DATABASE=bataillenavale; UID=root; PASSWORD=";
            this.connection = new MySqlConnection(connectionString);
            try
            {
                // Ouverture de la connexion SQL
                this.connection.Open();

                // Création d'une commande SQL en fonction de l'objet connection
                MySqlCommand cmd = this.connection.CreateCommand();

                // Requête SQL
                cmd.CommandText = "SELECT * FROM scores";

                // Exécution de la commande SQL
                cmd.ExecuteNonQuery();

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    scores.Add(reader.GetInt32("resultat"));
                }
                scores.Sort();

                if (scores[0] != null)
                    best.Text = "Meilleur score : " + scores[0];
                if(scores[1] != null)
                    best2.Text = "Second meilleur score : " + scores[1];
                if (scores[2] != null)
                    best3.Text = "Troisème meilleur score : " + scores[2];

                // Fermeture de la connexion
                this.connection.Close();
            }
            catch
            {

            }
        }

        private void retour_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu();
            this.Hide();
            menu.Show();
        }
    }
}
